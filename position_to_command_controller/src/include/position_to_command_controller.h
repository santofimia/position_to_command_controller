#ifndef position_to_command_controller
#define position_to_command_controller

#include "pid_control.h"

/*!*************************************************************************************
 *  \class     position_to_command_controller
 *
 *  \brief     Position to Command Controller PID
 *
 *  \details   This class is in charge of control position XY using PID class.
 *             Sends as Output commands to midLevel Control.
 *
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include <ros/ros.h>

#include "xmlfilereader.h"

class PostoCmdController
{
public:

  void setUp();

  void start();

  void stop();

  void getOutput(float *pitchb, float *rollb, float yaw);
  void setReference(float ref_x, float ref_y);
  void setFeedback(float x, float y);

  //! Read Config
  bool readConfigs(std::string configFile);

  //! Constructor. \details Same arguments as the ros::init function.
  PostoCmdController();

  //! Destructor.
  ~PostoCmdController();

private:

  PID PID_X2Pitch;
  PID PID_Y2Roll;

  //! Configuration file variable
  int idDrone;               // Id Drone integer (number of the drone)
  std::string pos2cmd_config_file;    // Config File String name
  std::string stackPath;     // Config File String aerostack path name

  // Output
  float pitch;
  float roll;

  // Gains

//  float pid_x2pitch_1_kp;
//  float pid_x2pitch_1_ki;
//  float pid_x2pitch_1_kd;
  bool pid_x2pitch_enablesat;
  float pid_x2pitch_satmax;
  float pid_x2pitch_satmin;
  bool pid_x2pitch_enableantiwp;
  float pid_x2pitch_kw;

  float pid_x2pitch_2_kp;
  float pid_x2pitch_2_ki;
  float pid_x2pitch_2_kd;


//  float pid_y2roll_1_kp;
//  float pid_y2roll_1_ki;
//  float pid_y2roll_1_kd;
  bool pid_y2roll_enablesat;
  float pid_y2roll_satmax;
  float pid_y2roll_satmin;
  bool pid_y2roll_enableantiwp;
  float pid_y2roll_kw;

  float pid_y2roll_2_kp;
  float pid_y2roll_2_ki;
  float pid_y2roll_2_kd;

};
#endif
