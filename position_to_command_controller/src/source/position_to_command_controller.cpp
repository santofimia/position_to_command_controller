#include "position_to_command_controller.h"

//Constructor
PostoCmdController::PostoCmdController()
{
    std::cout << "Constructor: PostoCmdController" << std::endl;

}

//Destructor
PostoCmdController::~PostoCmdController() {}



bool PostoCmdController::readConfigs(std::string configFile)
{

    try
    {


    XMLFileReader my_xml_reader(configFile);


    /*********************************  Speed X Controller ( from Dx to Pitch ) ************************************************/

    // Gain
//    pid_x2pitch_1_kp = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_X2Pitch:Gain1:Kp");
//    pid_x2pitch_1_ki = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_X2Pitch:Gain1:Ki");
//    pid_x2pitch_1_kd = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_X2Pitch:Gain1:Kd");

    pid_x2pitch_2_kp = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_X2Pitch:Gain2:Kp");
    pid_x2pitch_2_ki = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_X2Pitch:Gain2:Ki");
    pid_x2pitch_2_kd = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_X2Pitch:Gain2:Kd");

    pid_x2pitch_enablesat = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_X2Pitch:Saturation:enable_saturation");
    pid_x2pitch_satmax = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_X2Pitch:Saturation:SatMax");
    pid_x2pitch_satmin = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_X2Pitch:Saturation:SatMin");
    pid_x2pitch_enableantiwp = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_X2Pitch:Anti_wind_up:enable_anti_wind_up");
    pid_x2pitch_kw = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_X2Pitch:Anti_wind_up:Kw");


    /*********************************  Speed Y Controller ( from Dy to Roll ) **************************************************/

    // Gain
//    pid_y2roll_1_kp = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_Y2Roll:Gain1:Kp");
//    pid_y2roll_1_ki = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_Y2Roll:Gain1:Ki");
//    pid_y2roll_1_kd = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_Y2Roll:Gain1:Kd");

    pid_y2roll_2_kp = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_Y2Roll:Gain2:Kp");
    pid_y2roll_2_ki = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_Y2Roll:Gain2:Ki");
    pid_y2roll_2_kd = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_Y2Roll:Gain2:Kd");

    pid_y2roll_enablesat = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_Y2Roll:Saturation:enable_saturation");
    pid_y2roll_satmax = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_Y2Roll:Saturation:SatMax");
    pid_y2roll_satmin = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_Y2Roll:Saturation:SatMin");
    pid_y2roll_enableantiwp = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_Y2Roll:Anti_wind_up:enable_anti_wind_up");
    pid_y2roll_kw = my_xml_reader.readDoubleValue("Pos2Cmd_Controller:PID_Y2Roll:Anti_wind_up:Kw");

    }


    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }


    return true;
}

void PostoCmdController::setUp()
{

    ros::param::get("~stackPath", stackPath);
    if ( stackPath.length() == 0)
    {
        stackPath = "$(env AEROSTACK_STACK)";
    }
    ros::param::get("~droneId", idDrone);
    ros::param::get("~pos2cmd_config_file", pos2cmd_config_file);
    if ( pos2cmd_config_file.length() == 0)
    {
        pos2cmd_config_file="position_to_command_controller.xml";
    }

    bool readConfigsBool = readConfigs(stackPath+"/configs/drone"+cvg_int_to_string(idDrone)+"/"+pos2cmd_config_file);
    if(!readConfigsBool)
    {
        std::cout << "Error init"<< std::endl;
        return;
    }
    PID_X2Pitch.setDerFactor(0.02);
    PID_Y2Roll.setDerFactor(0.02);
    std::cout << "Constructor: PositiontoCommandController...Exit" << std::endl;

}

void PostoCmdController::start()
{

    // Reset PID
    PID_X2Pitch.reset();
    PID_Y2Roll.reset();

}

void PostoCmdController::stop()
{

}


void PostoCmdController::setFeedback(float x, float y){
    PID_X2Pitch.setFeedback(x);
    PID_Y2Roll.setFeedback(y);
}
void PostoCmdController::setReference(float ref_x, float ref_y){

    PID_X2Pitch.setReference(ref_x);
    PID_Y2Roll.setReference(ref_y);
}

void PostoCmdController::getOutput(float *pitchb, float *rollb, float yaw){


    /*********************************  Speed X Controller ( from Dx to Pitch ) ************************************************/

//    float error;
//    error = PID_X2Pitch.getError();
//    if (abs(error) < 0.75){
//        PID_X2Pitch.setGains(pid_x2pitch_1_kp,pid_x2pitch_1_ki,pid_x2pitch_1_kd);
//      }
//    else{
        PID_X2Pitch.setGains(pid_x2pitch_2_kp,pid_x2pitch_2_ki,pid_x2pitch_2_kd);
//      }

    PID_X2Pitch.enableMaxOutput(pid_x2pitch_enablesat,pid_x2pitch_satmin,pid_x2pitch_satmax);
    PID_X2Pitch.enableAntiWindup(pid_x2pitch_enableantiwp,pid_x2pitch_kw);

    pitch = -PID_X2Pitch.getOutput();


    /*********************************  Speed Y Controller ( from Dy to Roll ) **************************************************/

//    error = PID_Y2Roll.getError();
//    if (abs(error) < 0.75){
//        PID_Y2Roll.setGains(pid_y2roll_1_kp,pid_y2roll_1_ki,pid_y2roll_1_kd);
//      }
//    else
      PID_Y2Roll.setGains(pid_y2roll_2_kp,pid_y2roll_2_ki,pid_y2roll_2_kd);


    PID_Y2Roll.enableMaxOutput(pid_y2roll_enablesat,pid_y2roll_satmin,pid_y2roll_satmax);
    PID_Y2Roll.enableAntiWindup(pid_y2roll_enableantiwp,pid_y2roll_kw);


    roll = PID_Y2Roll.getOutput();


    /******************************** World to Body Transformation ********************************************/



//    if ((abs(PID_X2Pitch.getError())<0.25) && abs(PID_Y2Roll.getError())<0.25){
//        pitch = 0.0;
//        roll = 0.0;
//        PID_X2Pitch.reset();
//        PID_Y2Roll.reset();
//      }
    *pitchb =  pitch * cos (yaw) - roll * sin (yaw);
    *rollb  =  - pitch * sin (yaw) - roll * cos (yaw);

}

